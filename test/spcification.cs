﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using specificator;

namespace test
{
    [TestClass]
    public class specification
    {
        [TestMethod]
        public void swagger()
        {
            var d = AppDomain.CurrentDomain.BaseDirectory;
            d = d.Replace("\\test\\bin\\Debug", "");
            var path = Path.Combine(d, "mock_mq_node\\bin\\Debug\\mock_mq_node.exe");

            var s = new performer();
            var spec = s.swagger(path, "1.0.1");
        }
    }
}
