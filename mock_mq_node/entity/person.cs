﻿namespace mock_mq_node.entity
{
    /// <summary>
    /// сущность персоны
    /// </summary>
    public class person
    {
        /// <summary>
        /// имя
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// фамилия
        /// </summary>
        public string lname { get; set; }

        /// <summary>
        /// адрес
        /// </summary>
        public address address { get; set; }

    }
}
