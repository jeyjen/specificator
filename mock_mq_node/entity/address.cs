﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mock_mq_node.entity
{
    public class address
    {
        /// <summary>
        /// дом
        /// </summary>
        public int home { get; set; }
        /// <summary>
        /// улица
        /// </summary>
        public string street { get; set; }
    }
}
