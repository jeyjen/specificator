﻿using jeyjen.mq_node;
using mock_mq_node.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mock_mq_node.api
{
    /// <summary>
    /// описание контейнера index
    /// </summary>
    /// <see href="http://index.ru"/>
    public class index : container
    {
        /// <summary>
        /// описание метода index.get
        /// </summary>
        /// <param name="p0">desc p0</param>
        /// <param name="p1">desc p1</param>
        /// <param name="p2">desc p2</param>
        /// <param name="p3">desc p3</param>
        /// <param name="p4">desc p4</param>
        /// <param name="p5">desc p5</param>
        /// <param name="p6">desc p6</param>
        /// <param name="p7">desc p7</param>
        /// <param name="p8">desc p8</param>
        /// <param name="p9">desc p9</param>
        /// <param name="p10">desc p10</param>
        /// <param name="p11">desc p11</param>
        /// <param name="p12">desc p12</param>
        /// <param name="p13">desc p13</param>
        /// <param name="p14">desc p14</param>
        /// <param name="p15">desc p15</param>
        /// <param name="p16">desc p16</param>
        /// <param name="p17">desc p17</param>
        /// <param name="p18">desc p18</param>
        /// <param name="op1">desc op1</param>
        /// <param name="r1">desc r1</param>
        public void get(
            require<bool> p0,
            require<string> p1,
            require<sbyte> p2,
            require<byte> p3,
            require<char> p4,
            require<short> p5,
            require<ushort> p6,
            require<int> p7,
            require<uint> p8,
            require<long> p9,
            require<ulong> p10,
            require<float> p11,
            require<double> p12,
            require<decimal> p13,
            require<int[]> p14,
            require<person[]> p15,
            require<List<person>> p16,
            require<List<int>> p17,
            require<HashSet<int>> p18,
            //require<Dictionary<string, int>> p19,

            optional<string> op1, 
            output<string> r1)
        {}
    }
}
