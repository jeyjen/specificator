﻿using System.Collections.Generic;

namespace specificator
{
    public class details
    {
        public details()
        {
            href = "";
            summary = "";
            returns = "";
            members = new Dictionary<string, string>();
        }

        public string href { get; set; }
        public string summary { get; set; }
        public Dictionary<string, string> members { get; set; }
        public string returns { get; internal set; }
    }
}
