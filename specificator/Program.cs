﻿using jeyjen.extension;
using System;
using System.Collections.Generic;
using System.IO;

namespace specificator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var a = new Dictionary<string, string>();
                for (var i = 0; i < args.Length; i += 2)
                    a.Add(args[i].Substring(1), args[i + 1]);

                if (!a.ContainsKey("a"))
                    throw new Exception("\"a\" (assembly) is required");

                var path = a["a"];
                if (!Path.IsPathRooted(a["a"]))
                    path = Path.Combine(Directory.GetCurrentDirectory(), path);
                
                var p = new performer();

                var version = ":)";
                if (a.ContainsKey("v"))
                    version = a["v"];


                var spec = p.swagger(path, version);
                var save_path = Directory.GetCurrentDirectory();
                if (a.ContainsKey("o"))
                {
                    save_path = a["o"];
                }
                save_path = Path.GetFullPath(save_path);
                var file = Path.Combine(save_path, "{0}.json".format(p.name));
                Console.WriteLine("saved to \"{0}\"", file);
                if (File.Exists(file))
                    File.Delete(file);

                var sw = File.CreateText(file);
                sw.Write(spec);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        
    }
}
